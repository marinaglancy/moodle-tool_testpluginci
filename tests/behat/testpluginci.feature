@tool @tool_testpluginci
Feature: Test for plugin testpluginci
  In order to test running behat in gitlab and github 
  I need to have at least one behat test 

  Scenario: A test that does nothing but only tests that it passes without javascript tag
    Given I log in as "admin"
    And I am on homepage

  @javascript
  Scenario: A test that does nothing but only tests that it passes with javascript tag
    Given I log in as "admin"
    And I am on homepage
